FROM golang:1.22-alpine

# Create non-root user
RUN addgroup -g 1000 snippetbox && adduser -D -u 1000 -G snippetbox gouser

# Set working dir
WORKDIR /snippetbox

# Copy app src files
COPY . /snippetbox

# Set permissions files to the app user
RUN chown -R gouser:snippetbox /snippetbox

# change to the app user
USER gouser

# Port
EXPOSE 8000

# RUN app
ENTRYPOINT [ "go" ]
CMD ["run", "./cmd/web"]