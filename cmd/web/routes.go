package main

import "net/http"

// The routes() method returns a servemux containing the application routes.
func (app *application) routes() *http.ServeMux {
	mux := http.NewServeMux()
	// Create a file server to serves files out of the "./ui/static" directory.
	fileServer := http.FileServer(neuteredFileSystem{http.Dir("./ui/static")})
	// Disable fileserver directory listings
	mux.Handle("/static", http.NotFoundHandler())
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))
	// Register rest of the routes
	mux.HandleFunc("GET /{$}", app.home)
	mux.HandleFunc("GET /snippet/view/{id}", app.snippetView)
	mux.HandleFunc("GET /snippet/create", app.snippetCreate)
	mux.HandleFunc("POST /snippet/create", app.snippetCreatePost)
	return mux
}
