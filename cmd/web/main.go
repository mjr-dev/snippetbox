package main

import (
	"flag"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
)

// Application struct to hold application-wide dependencies
type application struct {
	logger *slog.Logger
}

// Filesystem struct for filesystem handler
type neuteredFileSystem struct {
	fs http.FileSystem
}

// Custom filesystem handler to disable filserver directory listings
func (nfs neuteredFileSystem) Open(path string) (http.File, error) {
	f, err := nfs.fs.Open(path)
	if err != nil {
		return nil, err
	}

	s, err := f.Stat()
	if s.IsDir() {
		index := filepath.Join(path, "index.html")
		if _, err := nfs.fs.Open(index); err != nil {
			closeErr := f.Close()
			if closeErr != nil {
				return nil, closeErr
			}

			return nil, err
		}
	}

	return f, nil
}

func main() {
	// Command-Line flag definition named 'addr' for http server address
	// default value of ":4000"
	addr := flag.String("addr", ":8000", "HTTP network address")
	flag.Parse()

	// Creating structured logs with 'log/slog' package
	// Initializing new structured logger to write to standart out stream
	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))

	// Initialize new instance of application struct, that has the app dependencies
	app := &application{
		logger: logger,
	}

	// Value returned from 'flag.String()' function is a pointer to the flag value
	// Need to use the dereference '*' operator to get teh actual value
	logger.Info("starting server", "addr", *addr)
	err := http.ListenAndServe(*addr, app.routes())
	// Log any error messages returned by 'http.ListenAndServe'
	logger.Error(err.Error())
	os.Exit(1)
}
